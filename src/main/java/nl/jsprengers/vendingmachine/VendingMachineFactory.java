package nl.jsprengers.vendingmachine;

public class VendingMachineFactory {

    public static VendingMachine createAmericanSodaMachine() {
        return new AmericanSodaMachine();
    }
}
