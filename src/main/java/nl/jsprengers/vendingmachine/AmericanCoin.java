package nl.jsprengers.vendingmachine;

public enum AmericanCoin {
    PENNY_1(1), NICKLE_5(5), DIME_10(10), QUARTER_25(25);

    private final int denomination;

    AmericanCoin(int denomination){
        this.denomination = denomination;
    }

    public int getDenomination(){
        return denomination;
    }
}
