# The Vending Machine Tutorial

Adapted from [this blog post](https://javarevisited.blogspot.com/2016/06/design-vending-machine-in-java.html#axzz4sZVwtCgs)

Look here for the [Kotlin version](https://gitlab.com/jasper-sprengers/vendingmachine-tutorial-kotlin)

## Set up your development environment and running the example
To work with the application you need a JDK, version 17 or higher and Apache maven. In IntelliJ these are built-in.
The following command in the root of this project will compile and test the project. Unit tests are not present yet: we will add those in subsequent sessions
```bash
mvn package
```

## Purpose of the exercise
The vending machine exercise is an introduction to Agile software development by using a small imperfect prototype of an application and making incremental improvements.
The code is simple, yet not entirely trivial and everybody is familiar with the use case of operating a vending machine.


## Steps towards improvement
The changes we will make to the code can be broken down into several types of effort. In each exercise we will probably tackle more than one  
* Writing and maintaining unit tests
* improvements to the readability/quality of the code and replacing certain syntactic construction with more modern ones. This is called refactoring: improvements that do not change the existing functionality of the code for the user.
* Fixing inconsistencies and bugs.
* Adding new features

There is a necessary order in which we should tackle all these issues. First comes automated testing.
Right now the project has not a single test to validate that the code works correctly. That is a terrible basis for making changes.
You can only do refactoring if you have the confidence that your changes keep the code working as it should and you don't introduce bugs by accident.  So for the first exercise we will put on our tester's hat and have a very good look at the `VendingMachineAPI` interface.

## Designing test cases 

It looks very simple at first glance: there are only four methods with which to interact:
* select an item and see how much it costs
* insert one or more coins 
* collect your chosen item and receive change
* cancel your order and take your money back

The object implemented by the VendingMachineAPI is what we call stateful: it has coins and cans inside and we can manipulate the state by adding and removing coins and receiving cans of soda.
It does not have an inexhaustible supply of either. Sometimes there is not enough change to give back or an item is sold out. Therefore, some methods can throw exceptions.

The test scenarios that spring to mind first are the happy flow scenarios: you pick your item, insert coins and receive your item plus change (if applicable).

But what happens when we don't stick to the expected order of calling the methods? What happens when you try to collect and item without selecting it first, or inserting coins?
How can you simulate the situation that there is no more change left, or an item is sold out. It's very important that the machine should never steal from the user: they either get the item of choice, or their money back.

In unit tests, we like to formulate our test cases in the given/when/then pattern. You will also find it called arrange/act/assert. T

```
GIVEN: there is enough change and items available
WHEN: I select a COKE and insert 25 cents and select my item
THEN: I get my beverage and no change
```

The __given__ part is the state that the system should be in for us to successfully run our test case. We __arrange__ the system in a certain way.
The __when__ is the actual interaction (__act__) with the system, producing a result.
And __then__ we check (__assert__) that the result matches our expectation.

Try to find as many meaningful scenarios as possible, and focus especially on the 'unhappy' flow. Try to find faults in the system by making sure that you are prevented from doing things that are not allowed.

```
GIVEN: there is enough change and items available
WHEN: I collect my item
THEN: I should get a warning that I have not selected an item yet
```




